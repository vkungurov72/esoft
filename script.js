const fieldStandart = [null,null,null,null,null,null,null,null,null];
const area = document.getElementById('area');                   
const cells = area.getElementsByClassName('game-field_cell');  
const buttonReset = document.getElementById('reset-button');
const modal = document.getElementById('modal');
const exitModal = document.getElementById('exit')
const buttonMoveToBack = document.getElementById('moveToback');
let lastStep = '.';
const rememberSteps = [];
const player1 = {name:'Алексей'};
const player2 = {name:'Вадим'}; 
let counter = 0;

const enumPlayerPicture = {
    player1:'./img/kr.svg',
    player2:'./img/null.png'
}

class GameField {
    firstStepflagInator = true;
    pos = -1;
    field = [null,null,null,null,null,null,null,null,null];
    winPosition = [
        [0,1,2],//
        [3,4,5],
        [6,7,8],
        [0,3,6],//
        [1,4,7],
        [2,5,8],
        [0,4,8],//
        [2,4,6]
    ]
    players = []
    symbols = [enumPlayerPicture.player1,enumPlayerPicture.player2]
    constructor(player1,player2) {
        this.players.push(player1);
        this.players.push(player2);
    }
     makeMove (position,symbol) { 
        this.field[position] = symbol;
    }

    checkCombinationIsWin (symbol) {
        let isWin = true;
        for (const positio of this.winPosition) {
            isWin = true
            for (const cell of positio) {
                isWin = isWin && this.field[cell] === symbol;
            }   
            if (isWin){
                return true;
            }
         }

        return false;
    }
    getSymbol (idx) {
        return this.symbols[idx];
    }
    getPlayer (idx) {
        return this.players[idx];
    }
  
}


let game = new GameField(player1,player2);

for (let i =0; i<9;i++){
    cells[i].addEventListener('click', checkCombination)
}

function checkCombination () {
    counter++
    if (game.firstStepflagInator) {
        var i = 0;
        game.firstStepflagInator = false;
    } else {
        var i = 1;
        game.firstStepflagInator = true;
    }
    const currentPlayerSymbol = game.getSymbol(i%2);
    this.innerHTML = "<img src="+currentPlayerSymbol+"></img>"
    this.style.pointerEvents = "none";
    game.makeMove(this.getAttribute('pos'),currentPlayerSymbol);
    if (game.checkCombinationIsWin(currentPlayerSymbol)) {
         unClickable()
        modal.style.display = 'flex';
        let winner = modal.querySelector("#nameWinner")
        winner.textContent = `Победил ${game.getPlayer(i%2).name}`
    } else if (counter == game.field.length | !game.field.includes(null)) {
        modal.style.display = 'flex';
        let winner = modal.querySelector("#nameWinner")
        winner.textContent = `Ничья!`
    }
    lastStep = this.getAttribute('pos')
    rememberSteps.push(lastStep);
}


function unClickable () {
    for (let i =0; i<9;i++){
        cells[i].style.pointerEvents = "none";
    }
}

buttonReset.addEventListener('click',reset)
function reset() {
    game.field = fieldStandart;
    game.firstStepflagInator = true;
    counter = 0;
    for (let i =0; i<9;i++){
        cells[i].style.pointerEvents = "auto";
        cells[i].innerHTML = ""
    }
    game = new GameField(player1,player2)
}


exitModal.addEventListener('click', function () {
    modal.style.display = 'none';
})

buttonMoveToBack.addEventListener('click',makeToMoveBack)
function makeToMoveBack () {
    counter--;
    if (rememberSteps.length ) {
        const deleteItem = rememberSteps.splice(-1,1);
        game.field[deleteItem] = null
        game.firstStepflagInator = !game.firstStepflagInator
    cells[deleteItem].innerHTML = ""
    cells[deleteItem].style.pointerEvents = "auto"
    for (let n in game.field){
        if (game.field[n] == null) {
            cells[n].style.pointerEvents = "auto"
        }
    }
    }  
}



